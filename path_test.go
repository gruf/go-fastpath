package fastpath_test

import (
	stdpath "path"
	"testing"

	"codeberg.org/gruf/go-fastpath/v2"
)

var testPaths = []string{
	"",
	"home/user",
	"/home/user",
	"/root",
	"/home/user/../user2",
	"../../ham",
	"../../../../../../../../",
	".",
	"//",
	"/home/user/Downloads/folder/../../",
	"home/user/../../var/opt/../../home/user/Downloads/../../../../../root/",
	"/.",
	"/m",
	"m/",
	"/m/",
	"/./",
	"./",
	"/../",
	"../",
	"/..",
	"/.bashrc",
	".bashrc",
	".e",
	"e.",
	"..bashrc",
	"bashrc..",
	"../path/../path/../path/../path/../path/../path/../",
	"/path/../path/../path/../path/../path/../path/../path/",
	"\x00\n\r\t\v\f\a\b",
}

var (
	benchPath1 = "/home/user/Downloads/folder/../../"
	benchPath2 = "home/user/../../var/opt/../../home/user/Downloads/../../../../../root/"
)

func testClean(t *testing.T, p string) {
	t.Logf("testClean: %s", p)
	test := fastpath.Clean(p)
	expect := stdpath.Clean(p)
	if test != expect {
		t.Errorf("Failed: expect = \"%s\", result = \"%s\"", expect, test)
	}
}

func testJoin(t *testing.T, p1, p2 string) {
	t.Logf("testJoin: %s %s", p1, p2)
	test := fastpath.Join(p1, p2)
	expect := stdpath.Join(p1, p2)
	if test != expect {
		t.Errorf("Failed: expect = \"%s\", result = \"%s\"", expect, test)
	}
}

func testAbsolute(t *testing.T, p string) {
	builder := fastpath.Builder{B: make([]byte, 0, 512)}

	t.Logf("testAbsolute: %s", p)
	builder.Append(p)
	path := stdpath.Clean(p)
	test := builder.Absolute()
	expect := stdpath.IsAbs(path)
	if test != expect {
		t.Errorf("Failed: expect = \"%v\", result = \"%v\" -- %s", expect, test, builder.String())
	}
}

func testSetAbsolute(t *testing.T, p string) {
	builder := fastpath.Builder{B: make([]byte, 0, 512)}

	t.Logf("testAbsolute: %s", p)
	builder.Append(p)

	before := builder.Absolute()
	if before {
		builder.SetAbsolute(false)
	} else {
		builder.SetAbsolute(true)
	}

	test := builder.Absolute()
	expect := stdpath.IsAbs(builder.String())

	if test != expect {
		t.Errorf("Failed: expect = \"%v\", result = \"%v\" -- %q", expect, test, builder.String())
	}
}

func TestClean(t *testing.T) {
	for _, p := range testPaths {
		testClean(t, p)
	}
}

func TestJoin(t *testing.T) {
	for _, p1 := range testPaths {
		for _, p2 := range testPaths {
			testJoin(t, p1, p2)
		}
	}
}

func TestAbsolute(t *testing.T) {
	for _, p := range testPaths {
		testAbsolute(t, p)
	}
}

func TestSetAbsolute(t *testing.T) {
	for _, p := range testPaths {
		testSetAbsolute(t, p)
	}
}

func BenchmarkBuilderClean(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			fastpath.Clean(benchPath1)
			fastpath.Clean(benchPath2)
		}
	})
}

func BenchmarkStdPathClean(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			stdpath.Clean(benchPath1)
			stdpath.Clean(benchPath2)
		}
	})
}

func BenchmarkBuilderJoin(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			fastpath.Join(benchPath1, benchPath2)
			fastpath.Join(benchPath2, benchPath1)
		}
	})
}

func BenchmarkStdPathJoin(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			stdpath.Join(benchPath1, benchPath2)
			stdpath.Join(benchPath2, benchPath1)
		}
	})
}
